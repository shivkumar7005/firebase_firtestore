package com.firestore;

import com.firestore.controller.LoginController;
import com.firestore.initialize.InitializeApp;

import javafx.application.Application;
import javafx.stage.Stage;

public class Main {

    public static void main(String[] args) {
        Application.launch(InitializeApp.class,args);
    }
 
}
